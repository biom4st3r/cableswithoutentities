package com.biom4st3r.cablenetwork;

import net.fabricmc.api.ModInitializer;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModInit implements ModInitializer {
	public static final String MODID = "cablenetwork";

	public static Item PipeItem;
	public static final BaseCable PipeBlock = new BaseCable();

	@Override
	public void onInitialize() {

		PipeItem = Registry.ITEM.add(new Identifier(MODID,"basecable"), new BlockItem(PipeBlock, new Item.Settings().group(ItemGroup.MISC)));
		Registry.BLOCK.add(new Identifier(MODID, "basecable"), PipeBlock);

	}
}
