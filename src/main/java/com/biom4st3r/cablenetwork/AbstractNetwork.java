// package com.biom4st3r.cablenetwork;

// import com.google.common.graph.Graph;
// import com.google.common.graph.GraphBuilder;

// import org.apache.commons.lang3.NotImplementedException;

// import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
// import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
// import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
// import nerdhub.cardinal.components.api.ComponentType;
// import nerdhub.cardinalenergy.DefaultTypes;
// import nerdhub.cardinalenergy.api.IEnergyHandler;
// import net.minecraft.block.entity.BlockEntity;
// import net.minecraft.util.math.Direction;
// import net.minecraft.world.dimension.DimensionType;

// /**
//  * Network
//  */
// public class AbstractNetwork implements IEnergyHandler {

//     protected DimensionType type;
//     public LongOpenHashSet nodes = new LongOpenHashSet();
//     protected Long2ObjectMap<BlockEntity> endpoints = new Long2ObjectOpenHashMap<>();
    

//     public boolean addNode(long l) {
//         return this.nodes.add(l) && CableNetwork.cableNetworks.get(type).put(l, this) != null;
//     }

//     public boolean addEndPoint(BlockEntity be)
//     {
//         throw new NotImplementedException("not implemented");
//     }

//     public boolean removeNode(long l) {
//         if (this.nodes.remove(l)) {
//             return CableNetwork.cableNetworks.get(type).remove(l) != null;
//         }
//         return false;
//     }

//     public boolean removeEndPoint(BlockEntity be)
//     {
//         throw new NotImplementedException("not implemented");
//     }

//     public boolean contains(long pos) {
//         return nodes.contains(pos);
//     }

//     public void merge(AbstractNetwork oldnetwork) {
//         this.nodes.addAll(oldnetwork.nodes);
//         this.endpoints.putAll(oldnetwork.endpoints);
//         CableNetwork.changeNetwork(type, oldnetwork, this);
//     }
//     @Override
//     public boolean isEnergyReceiver(Direction direction, ComponentType componentType) {
//         return componentType==DefaultTypes.CARDINAL_ENERGY;
//     }
// }