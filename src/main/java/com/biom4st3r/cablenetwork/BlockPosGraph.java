package com.biom4st3r.cablenetwork;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;

import org.apache.commons.lang3.NotImplementedException;

import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

public class BlockPosGraph {
    public static Logger logger = Logger.getLogger("BlockPosGraph");
    static
    {
        logger.setLevel(Level.ALL);
    }
    public static final int DEFAULT_SIZE = 20;
    LongOpenHashSet nodes = new LongOpenHashSet(DEFAULT_SIZE);
    Long2ObjectMap<Node> connections = new Long2ObjectOpenHashMap<>(DEFAULT_SIZE);

    public static class Node implements Cloneable
    {
        /**Blockpos in world as long */
        long host;
        LongOpenHashSet neighbors = new LongOpenHashSet(6);
        
        public Node(long newNode) {
            this.host = newNode;
        }

        public boolean useless()
        {
            return this.neighbors.size() < 2;
        }

        public boolean connectedTo(long pos)
        {
            // Should return false if it is host?
            return host == pos || neighbors.contains(pos);
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            for(long l : neighbors)
            {
                builder.append(this.hashCode()).append(BlockPos.fromLong(host).toShortString()).append("->").append(BlockPos.fromLong(l).toShortString()).append("\n");
            }
            return builder.toString();
        }
        @Override
        public boolean equals(Object obj) {
            try
            {
                return this.host == ((Node)obj).host;
            }
            catch(ClassCastException e)
            {
                logger.log(Level.WARNING, "Compared some non-Node object to a Node");
                throw e;
            }
        }

        @Override
        protected Object clone() {
            Node node = new Node(this.host);
            node.neighbors = this.neighbors.clone();
            return node;
        }
    }
    /**
     * must allow newNode to be tracked<p>
     * must combine networks if linked by adding this block<p>
     * 
     * @param newNode
     * @return
     */
    public boolean addNode(long newNode, BlockState state)
    {
        if(this.nodes.contains(newNode)) return false;
        Node node = new Node(newNode);
        for(Direction dir : Direction.values())
        {
            if(state.get(BaseCable.properties.get(dir))) // TODO: Needs to only have nodes not endpoints
            {
                long neighbor = BlockPos.fromLong(newNode).offset(dir).asLong();
                node.neighbors.add(neighbor); // Needs to only have nodes not endpoints
                if(this.nodes.contains(neighbor))
                {
                    this.connections.get(neighbor).neighbors.add(newNode);
                }
                else
                {
                    throw new NotImplementedException("check if neighbor should be tracked or if neighbor is part of a different graph and merge the graphs");
                }
            }
        }
        this.connections.put(newNode, node);
        return true;
    }
    /**
     * must remove node from tracking<p>
     * must check if this splits the graph<p>
     * must update CableNetwork<p>
     * @param removedNode
     * @return
     */
    public boolean removeNode(long removedNode)
    {

        if(!this.nodes.contains(removedNode)) return false; // False if not in network

        Node removedConnection = connections.get(removedNode); // the Node to be removed from connections

        // Removing References
        this.connections.remove(removedNode);
        this.nodes.remove(removedNode);
        CableNetwork.removeNode(this,removedNode);
        // END

        List<Node> terminatedConnections = Lists.newArrayListWithCapacity(6); // list of neightboring node

        for(long l : removedConnection.neighbors) // Removing removedNode from all of it's neighbors
        {
            Node termConnection = connections.get(l);
            termConnection.neighbors.remove(removedNode);
            terminatedConnections.add(termConnection);
        }

        ListIterator<Node> iter0 = terminatedConnections.listIterator();
        ListIterator<Node> iter1 = terminatedConnections.listIterator();
        for(;iter0.hasNext();)
        {
            Node node0 = iter0.next();
            for(;iter1.hasNext();)
            {
                Node node1 = iter1.next();
                if(node0 == node1) continue;
                if(nodesConnected(node0, node1))
                {
                    iter1.remove();
                }
            }
        }
        if(terminatedConnections.size() > 1)
        {
            
            CableNetwork.handleNetworkSplit(split(terminatedConnections));
        }

        return true;
    }

    public Collection<BlockPosGraph> split(Collection<Node> networkStartingPoints)
    {
        List<BlockPosGraph> newGraphs = Lists.newArrayList();
        for(Node node : networkStartingPoints)
        {
            BlockPosGraph graph = new BlockPosGraph();
            graph.nodes.add(node.host);
            graph.connections.put(node.host, node);
            Queue<Long> neighbors = new ArrayDeque<>(nodes.size());
            neighbors.addAll(node.neighbors);
            LongSet blocker = new LongOpenHashSet(nodes.size());
            while(!neighbors.isEmpty())
            {
                long neighbor = neighbors.poll();
                blocker.add(neighbor);
                Node neighborNode = this.connections.get(neighbor);
                this.connections.remove(neighbor);
                graph.nodes.add(neighbor);
                graph.connections.put(neighbor, neighborNode);
                for(long neighborNeighbor : neighborNode.neighbors)
                {
                    if(!blocker.contains(neighborNeighbor))
                    {
                        neighbors.add(neighborNeighbor);
                    }
                }
            }
            newGraphs.add(graph);
        }

        return newGraphs;
    }

    public void merge()
    {
        throw new NotImplementedException("Merge BlockPosGraph");
    }

    public static class WeightedNode implements Comparable<WeightedNode>
    {
        public WeightedNode(Node node,double weight)
        {
            this.node = node;
            this.weight = weight;
        }
        Node node;
        double weight;

        @Override
        public int compareTo(WeightedNode o) {
            return this.weight < o.weight ? -1 : this.weight > o.weight ? 1 : 0;
        }
        

    }

    /**
     * Returns weather or not 2 node0 can reach eachother. Should have no side effects
     * @param node0
     * @param destination
     * @return
     */
    public boolean nodesConnected(final Node node0, final Node destination) // A*
    {
        Stopwatch sw = Stopwatch.createUnstarted();
        int counter = 0;
        // A*
        BlockPos dest = BlockPos.fromLong(destination.host);
        PriorityQueue<WeightedNode> openSet = Queues.newPriorityQueue(); // Probably a better collection to use.
        openSet.add(
            new WeightedNode(
                node0, 
                BlockPos.fromLong(node0.host).getSquaredDistance(dest)));
        LongOpenHashSet closeSet = new LongOpenHashSet(nodes.size()); // Faster lookup than adding a WeightedNode

        Map<WeightedNode,WeightedNode> cameFrom = Maps.newHashMapWithExpectedSize(nodes.size()); // Possibly unnessisary
        while(openSet.size() != 0)
        {
            sw.start();
            WeightedNode currentNode = openSet.poll();
            if(destination.host == currentNode.node.host || destination.neighbors.contains(currentNode.node.host))
            {
                logger.config(String.format("found in %s microsec", sw.stop().elapsed(TimeUnit.MICROSECONDS)));
                return true;
            }
            closeSet.add(currentNode.node.host);
            for(long l : currentNode.node.neighbors)
            {
                Node neightbor = connections.get(l);
                if(neightbor.useless()) continue; // save some cycle where you can.
                WeightedNode neighborNode = new WeightedNode(neightbor, BlockPos.fromLong(neightbor.host).getSquaredDistance(dest));
                if(neighborNode.weight < currentNode.weight)
                {
                    cameFrom.put(neighborNode, currentNode);
                    if(!closeSet.contains(neighborNode.node.host))
                    {
                        openSet.offer(neighborNode);
                    }
                }
            }
            sw.stop();
            logger.config(String.format("%s microsec per round for %s rounds", sw.elapsed(TimeUnit.MICROSECONDS)/(float)++counter,counter));
        }
        logger.config(String.format("failed in %s microsec", sw.stop().elapsed(TimeUnit.MICROSECONDS)));
        return false;
    }

    public Node getNode(long node)
    {
        return this.connections.get(node);
    }
	public void addEndPoint(BlockEntity blockEntity) {
        throw new NotImplementedException("message");
	}


}