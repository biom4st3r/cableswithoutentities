package com.biom4st3r.cablenetwork;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import com.ibm.icu.impl.Assert;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.tuple.Pair;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.BlockView;
import net.minecraft.world.dimension.DimensionType;

/**
 * CableNetwork
 */
public interface CableNetwork {
    static ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 3, 200, TimeUnit.MILLISECONDS,
            Queues.newArrayBlockingQueue(200));
    static Map<DimensionType, Long2ObjectOpenHashMap<BlockPosGraph>> cableNetworks = Maps.newHashMap();

    static void init() {
        Registry.DIMENSION_TYPE.forEach((dt) -> {
            cableNetworks.put(dt, new Long2ObjectOpenHashMap<>());
        });
    }

    static BlockPosGraph getNetwork(DimensionType dt,long l,BlockView view)
    {
        BlockPosGraph network = cableNetworks.get(dt).get(l);
        if(network==null)
        {
            networkDiscovery(view, BlockPos.fromLong(l),dt);
            return null;
        }
        return network;
    }
    
    static boolean isEndPoint(Block block)
    {
        //TODO Fix. Not all blockentities are energy accepters
        return block.hasBlockEntity();
    }

    static void networkDiscovery(BlockView view, BlockPos originPos,DimensionType dt) {
        executor.submit(()->
        {
            BlockPosGraph network = new BlockPosGraph();
            
            List<Pair<BlockState,Long>> newNetwork = new ArrayList<Pair<BlockState,Long>>(){
                private static final long serialVersionUID = 1L;
                public boolean add(Pair<BlockState,Long> l)
                {
                    if(!this.contains(l)) return super.add(l);
                    return false;
                }
            };

            
            BlockState origin = view.getBlockState(originPos);
            newNetwork.add(Pair.of(origin, originPos.asLong()));
            Assert.assrt(origin.getBlock() instanceof BaseCable);
            ListIterator<Pair<BlockState,Long>> iter = newNetwork.listIterator();
            while(iter.hasNext())
            {
                BlockPos currentPos = BlockPos.fromLong(iter.next().getRight()); 
                BlockState currentState = view.getBlockState(currentPos);
                BlockPos offsetPos;
                BlockState offsetState;
                for(Direction dir : Direction.values())
                {
                    if(currentState.get(BaseCable.properties.get(dir)))
                    {
                        offsetPos = currentPos.offset(dir);
                        offsetState = view.getBlockState(offsetPos);
                        if(offsetState.getBlock() instanceof BaseCable)
                        {
                            iter.add(Pair.of(offsetState, offsetPos.asLong()));
                        }
                        else if(isEndPoint(offsetState.getBlock()))
                        {
                            network.addEndPoint(view.getBlockEntity(offsetPos));
                        }
                    }
                }
            }
            newNetwork.forEach((l)-> network.addNode(l.getRight().longValue(),l.getLeft())); // To be resolved


        });
    }

    static BlockPosGraph getNetwork(DimensionType dt, BlockPos pos, BlockView view)
    {
        return getNetwork(dt, pos.asLong(),view);
    }


	static void handleNetworkSplit(Collection<BlockPosGraph> split) {
        for(BlockPosGraph graph : split)
        throw new NotImplementedException("handleNtworkSplit");
	}

	static void removeNode(BlockPosGraph blockPosGraph, long removedNode) {

	}

    
    

}