package com.biom4st3r.cablenetwork;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import com.biom4st3r.biow0rks.Biow0rks;
import com.google.common.collect.ImmutableMap;

import nerdhub.cardinal.components.api.ComponentType;
import nerdhub.cardinal.components.api.component.BlockComponentProvider;
import nerdhub.cardinal.components.api.component.Component;
import nerdhub.cardinalenergy.DefaultTypes;
import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.state.StateManager.Builder;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockView;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

/**
 * BasicPipeBlock
 */
public class BaseCable extends Block implements BlockComponentProvider {

    public BaseCable() {
        super(FabricBlockSettings.copy(Blocks.NETHER_BRICK_FENCE).hardness(1).nonOpaque().noCollision().breakInstantly()
                .build());
        this.setDefaultState(this.stateManager.getDefaultState().with(Properties.NORTH, false)
                .with(Properties.EAST, false).with(Properties.SOUTH, false).with(Properties.WEST, false)
                .with(Properties.UP, false).with(Properties.DOWN, false));
    }

    @Override
    protected void appendProperties(Builder<Block, BlockState> builder) {
        builder.add(Properties.NORTH, Properties.EAST, Properties.SOUTH, Properties.WEST, Properties.UP,
                Properties.DOWN);
    }

    @Override
    public void onBlockAdded(BlockState state, World world, BlockPos pos, BlockState oldState, boolean moved) {
        Biow0rks.logger.error("onBlockAdded");
        super.onBlockAdded(state, world, pos, oldState, moved);
    }

    @Override
    public BlockState getPlacementState(ItemPlacementContext ctx) {
        Biow0rks.logger.error("placed");
        World world = ctx.getWorld();
        BlockPos pos = ctx.getBlockPos();
        BlockState north = ctx.getWorld().getBlockState(ctx.getBlockPos().north());
        BlockState east = ctx.getWorld().getBlockState(ctx.getBlockPos().east());
        BlockState south = ctx.getWorld().getBlockState(ctx.getBlockPos().south());
        BlockState west = ctx.getWorld().getBlockState(ctx.getBlockPos().west());
        BlockState up = ctx.getWorld().getBlockState(ctx.getBlockPos().up());
        BlockState down = ctx.getWorld().getBlockState(ctx.getBlockPos().down());
        return super.getPlacementState(ctx)
                .with(Properties.NORTH, shouldConnect(north, world, pos.north(),Direction.SOUTH))
                .with(Properties.EAST, shouldConnect(east, world, pos.east(),Direction.WEST))
                .with(Properties.SOUTH, shouldConnect(south, world, pos.south(),Direction.NORTH))
                .with(Properties.WEST, shouldConnect(west, world, pos.west(),Direction.EAST))
                .with(Properties.UP, shouldConnect(up, world, pos.up(),Direction.DOWN))
                .with(Properties.DOWN, shouldConnect(down, world, pos.down(),Direction.UP));
    }

    public boolean shouldConnect(BlockState state, IWorld world, BlockPos pos,Direction opposedDirection) {
        if (state.getBlock() == Blocks.AIR)
            return false;
        else if(state.getBlock() instanceof BaseCable)
        {
            return true;
        }
        else if (state.getBlock() instanceof BlockComponentProvider) {
            if(((BlockComponentProvider)state.getBlock()).hasComponent(world, pos, DefaultTypes.CARDINAL_ENERGY, null))
            return true;
        }
        return false;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Map<Direction, BooleanProperty> properties = (Map) ImmutableMap.builder()
            .put(Direction.UP, Properties.UP).put(Direction.DOWN, Properties.DOWN)
            .put(Direction.NORTH, Properties.NORTH).put(Direction.EAST, Properties.EAST)
            .put(Direction.SOUTH, Properties.SOUTH).put(Direction.WEST, Properties.WEST).build();

    @Override
    public BlockState getStateForNeighborUpdate(BlockState state, Direction facing, BlockState neighborState,
            IWorld world, BlockPos pos, BlockPos neighborPos) {
        return state.with(properties.get(facing), shouldConnect(neighborState, world, pos,facing));
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand,
            BlockHitResult hit) {

        return ActionResult.SUCCESS;

    }

    @Override
    public <T extends Component> boolean hasComponent(BlockView blockView, BlockPos pos, ComponentType<T> type,
            Direction side) {
        return type == DefaultTypes.CARDINAL_ENERGY;
    }

    @Override
    public <T extends Component> T getComponent(BlockView blockView, BlockPos pos, ComponentType<T> type,
            Direction side) {
        return type == DefaultTypes.CARDINAL_ENERGY ? (T)CableNetwork.getNetwork(((World)blockView).dimension.getType(), pos, blockView) : null;
    }

    @Override
    public Set<ComponentType<?>> getComponentTypes(BlockView blockView, BlockPos pos, Direction side) {
        return Collections.singleton(DefaultTypes.CARDINAL_ENERGY);
    }

    
}