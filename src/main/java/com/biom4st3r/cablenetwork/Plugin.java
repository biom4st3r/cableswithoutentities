package com.biom4st3r.cablenetwork;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import com.biom4st3r.biow0rks.BioLogger;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

/**
 * Plugin
 */
public class Plugin implements IMixinConfigPlugin {

    Predicate<MethodNode> isFinal = (mv)->(mv.access & Opcodes.ACC_FINAL) != 0;

    // private static ClassReader newReader(String s)
    // {
    //     try {
    //         return new ClassReader(s);
    //     } catch (IOException e) {
    //         // TODO Auto-generated catch block
    //         e.printStackTrace();
    //     }
    //     return null;
    // }

    @Override
    public void onLoad(String mixinPackage) {
        // ClassReader reader = newReader("net/minecraft/block/Block");
        // ClassWriter cw = new ClassWriter(reader, ClassReader.EXPAND_FRAMES);
        // ClassNode classNode = new ClassNode(Opcodes.ASM7);
        // reader.accept(classNode, ClassReader.SKIP_CODE);
        // classNode.methods
        //     .stream()
        //     .filter(isFinal)
        //     .peek((mv)->mv.access &= ~Opcodes.ACC_FINAL)
        //     .collect(Collectors.toList())
        //     .forEach((mv)->
        //     {
        //         mv.accept(classNode.visitMethod(mv.access, mv.name, mv.desc, mv.signature, mv.exceptions.toArray(new String[0])));
        //     });
        // System.out.println();
        // classNode.accept(classNode);

    }

    @Override
    public String getRefMapperConfig() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
        return true;
    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) {

    }

    @Override
    public List<String> getMixins() {
        return null;
    }
    public static BioLogger logger = new BioLogger("ASM");
    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {

    }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {

    }

    
}